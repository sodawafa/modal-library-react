import React from 'react'
import styles from './styles.module.css'

class Modal extends React.Component {
  state = {
    modal: false
  }

  constructor(props) {
    super(props)

    if (this.props.modalFn) {
      this.openModal = this.openModal.bind(this)
      this.closeModal = this.closeModal.bind(this)
    } else {
      this.openModal = this.props.openModal
      this.closeModal = this.props.closeModal
    }
  }

  openModal() {
    this.setState({ modal: true })
  }

  closeModal() {
    this.setState({ modal: false })
  }

  render() {
    return (
      <div>
        {this.props.modalBtnOpen && (
          <button className={styles.modalOpen} onClick={this.openModal}>
            open modal
          </button>
        )}
        {((this.props.modalFn && this.state.modal) ||
          (!this.props.modalFn && this.props.modal)) && (
          <div className={styles.modalContainer} onClick={this.closeModal}>
            <div
              className={styles.modal}
              style={{ marginTop: this.props.top ? this.props.top : '20%' }}
            >
              <div className={styles.modalBody} style={this.props.style}>
                {this.props.children}
              </div>
              {this.props.modalBtnClose && (
                <button className={styles.modalClose} onClick={this.closeModal}>
                  Close
                </button>
              )}
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default Modal
