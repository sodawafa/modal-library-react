import React from 'react'
import 'modal-library/dist/index.css'
import Modal from 'modal-library'

const App = () => {
  return <Modal modalFn={true}
                modalBtnOpen={true}
                modalBtnClose={true}
                top={'10%'}
                style={{margin:0}}
  >
    <p>Modal React Library 😄</p>
  </Modal>
}

export default App
