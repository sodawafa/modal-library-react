# modal-library

> my first library

[![NPM](https://img.shields.io/npm/v/modal-library.svg)](https://www.npmjs.com/package/modal-library) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save modal-library
```

## Usage

```jsx
import React, { Component } from 'react'

import Modal from 'modal-library'
import 'modal-library/dist/index.css'

class Example extends Component {
  render() {
    return
    <Modal modalFn={true}
           modalBtnOpen={true}
           modalBtnClose={true}
    >
      <p>Modal React Library 😄</p>
    </Modal>
  }
}
```
or
```jsx
import React from 'react'
import Modal from './index'
import 'modal-library/dist/index.css'

class Example2 extends React.Component {
  state = {
    modal: false
  }

  constructor(props) {
    super(props)
    this.openModal = this.openModal.bind(this)
  }

  openModal() {
    this.setState({ modal: true })
  }

  closeModal() {
    this.setState({ modal: false })
  }

  render() {
    return (
      <div>
        <button className='modal-btn' onClick={this.openModal}>
          open modal
        </button>
        <Modal
          modalFn={false}
          modalBtnOpen={false}
          modalBtnClose={true}
          modal={this.state.modal}
          openModal={this.openModal.bind(this)}
          closeModal={this.closeModal.bind(this)}
          top={'20%'}
          style={{margin:0}}
        >
          <p>component with modal</p>
        </Modal>
      </div>
    )
  }
}

export default Example2

```
## Etape creation library react

> [Readme](https://github.com/transitive-bullshit/create-react-library)

## License

MIT © [Wafa SODA](https://gitlab.com/sodawafa/modal-library-react)
